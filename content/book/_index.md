---
title: The original book
date: 2022-06-13T20:55:37+01:00
draft: false
layout: "simple"

showDate : false
showDateUpdated : false
showHeadingAnchors : false
showPagination : false
showReadingTime : false
showTableOfContents : false
showLikes : false
showViews : false
showEdit : false
showTaxonomies : false 
showWordCount : false
showSummary : false
sharingLinks : false
showAuthor: false
---

The 1982 book is no longer in print, and second-hand versions are hard to come by at the price point that prof. van Dyke would have had in mind when he wrote *"... needs to be inexpensive enough that it is readily accessible to students"*. To make his work enjoyable for all, Parabolic press has made the below *pdf* version of the book freely available. Of course, it is not quite the same as a hard-copy that you can flip through, so I'd encourage you to take the opportunity if you can get your hands on one of those.

<!--- <iframe src="https://drive.google.com/file/d/0B5UvitjuXx-bVWdoMFQxUXlZek0/preview?resourceKey=0-6l7d0NYe7FY8muhx8pCL8A" width="100%" height="1000px" allow="autoplay"></iframe>-->
<iframe src="Album-Fluid-Motion-Van-Dyke.pdf" width="100%" height="1000px"></iframe> 
