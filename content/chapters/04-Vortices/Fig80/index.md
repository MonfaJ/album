---
title: "Fig 80. Starting vortex on a wedge"
date: 2023-07-04
draft: true
weight: 800
featured: true
tags: ["FVM", "Ansys Fluent", "Vortex", "Separation"]
authors:
  - "roeljaspars"
---

{{< katex >}}


*"A piston drives water with almost constant speed normal to the axis of a wedge of 30 degree semi-vertex angle. Neutrally bouyant dye is injected into the water from small holes in the wedge surface. The characteristic Reynolds number is of order 1000. The piston stops at 12.5 s, producing a stopping vortex in the last photograph."* Photograph by Pullin & Perry 1980

{{< slider "1sec.jpg" "1secS.jpg" "Experiment" "Simulation">}}
{{< slider "3sec.jpg" "3secS.jpg" "Experiment" "Simulation">}}
{{< slider "5sec.jpg" "5secS.jpg" "Experiment" "Simulation">}}
{{< slider "7sec.jpg" "7secS.jpg" "Experiment" "Simulation">}}
{{< slider "Featured.jpg" "9secS.jpg" "Experiment" "Simulation">}}
{{< slider "11sec.jpg" "11secS.jpg" "Experiment" "Simulation">}}
{{< slider "13sec.jpg" "13secUpdate.jpg" "Experiment" "Simulation">}}

## Theory
In order to derive the vorticity equation, first the governing equations of fluid motion need to be introduced. The governing equations for fluid motion are the conservation of mass, conservation of linear momentum and conservation of energy [[1]](https://www.comsol.com/multiphysics/fluid-flow-conservation-of-momentum-mass-and-energy). The conservation of mass is expressed with in following partial differential equation:

$$ \frac{\partial \rho}{\partial t} + \nabla \cdot (\rho \textbf{v}) = 0 $$

This is the so called continuity equation, where \\(\rho\\) is the density of the fluid, \\(t\\) is time, \\(\Vec{v}\\) is the velocity vector of the fluid and \\(\nabla\\) is the del operator. The sum of the partial derivatives with respect to the different directions is also called divergence (div) in mathematics. The divergence is a mathematical operator, which balances flows through a volume element in case of a flow field.

The conservation of momentum equation is also known as the Navier-Stokes equations. The Navier-Stokes equations in their vector form are:

$$ \frac{\partial \rho \textbf{v}}{\partial t} + \nabla (\rho \textbf{v} \cdot \textbf{v}) = -\nabla p + \nabla \cdot \tau  + F $$

where \\(p\\) is the pressure, \\(\tau\\) is the viscous stress tensor and \\(F\\) represents any external forces acting on the fluid (such as gravity).

The energy equation accounts for the conservation of energy in fluid flow. It includes terms for the internal energy, kinetic energy, and potential energy of the fluid, as well as the effects of heat transfer and work done on the fluid. The conservation of energy is described by the total energy equation:
$$ \frac{\partial}{\partial t}[\rho (e+\frac{1}{2}\textbf{v}^2)]+\nabla \cdot[\rho \textbf{v} (e+\frac{1}{2}\textbf{v}^2)] = \nabla \cdot (k\nabla T) + \nabla \cdot(-p \textbf{v}+\tau \cdot \textbf{v}) + \textbf{v}\cdot F + Q $$

where \\(e\\) is the total energy per unit mass, \\(k\\) is the thermal conductivity of the fluid, \\(T\\) is the temperature of the fluid and \\(Q\\) is the heat source term. The viscous stress tensor \\(\tau\\) depends on the fluid's viscosity and velocity gradients and can be further expanded based on the specific flow conditions. Much of computational fluid dynamics is devoted to selecting suitable approximations to the above equations, so that accurate results are obtained with a reasonable computational cost. Therefor these equations will be further simplified or modified depending on the assumptions made for the specific flow conditions and the fluid properties considered. In the case of the experiment shown in figure 80 of `An Album of Fluid Motion` [[2]](http://courses.washington.edu/me431/handouts/Album-Fluid-Motion-Van-Dyke.pdf), a few assumptions can be made to simplify these equations. A two dimensional flow can be considered of a homogeneous and incompressible fluid. The density and the viscosity of the fluid are both assumed to be uniform. 

For incompressible fluids the density does not change and is therefore constant over time. The partial derivative of the density with respect to time is therefore zero. For incompressible fluids, the continuity equation has therefore the following form:

$$ \nabla \cdot \textbf{v} = 0 $$

The linear momentum conservation for a Newtonian fluid is given by the following Navier-Stokes equations [[3]](http://www.diva-portal.org/smash/get/diva2:205082/fulltext01.pdf).

$$ \frac{\partial \textbf{v}}{\partial t} + (\textbf{v} \cdot \nabla)\textbf{v} = -(\frac{1}{\rho})\nabla p + \nu \nabla^2 \textbf{v} + F $$ 

where \\(\nu\\) is the kinematic viscosity defined as the ratio of the dynamic viscosity and the density of the fluid and \\(\nabla^2\\) is the Laplacian operator. The temperature can be solved for separately if information about the temperature field is desirable, but this is of no importance in this case. The next step is introduce vorticity and to derive the vorticity from these equations.

#### Vorticity 
Vorticity is a fundamental concept in fluid dynamics that describes the local rotation of fluid particles in a flow. Vorticity plays a crucial role in understanding various fluid phenomena, such as turbulence, boundary layers, and the formation of vortices. It is a vector quantity that represents the circulation and rotation of fluid elements within a fluid medium. In a flowing fluid, neighboring fluid particles move along different paths and at different velocities, resulting in relative motion between them. Mathematically, the vorticity vector is defined as the curl of the fluid velocity vector:

$$ \textbf{$\omega$} = \nabla \times \textbf{v} $$

where \\(\textbf{$\omega$}\\) is the vorticity vector, \\(\nabla \times\\) is the curl operator (representing the gradient), and \\(\textbf{v}\\) is the velocity vector of the fluid. The magnitude of the vorticity vector represents the local rate of rotation of the fluid particles and the direction of the vector corresponds to the axis of rotation. High vorticity indicates a strong rotation, while low vorticity indicates little or no rotation.

The derivation of the voricity equation involves taking the curl of the Navier-Stokes equations and manipulating the resulting equations. Applying the curl operator (\\(\nabla \times\\)) to both sides of the Navier-Stokes equations and assuming there are no external forces acting on the fluid, the following equation is obtained [[4]](/https://hal.science/hal-03608428/document):

$$ \nabla \times \Bigl[ \frac{\partial \textbf{v}}{\partial t} + (\textbf{v} \cdot \nabla)\textbf{v} \Bigr]= \nabla \times \Bigl[-(\frac{1}{\rho})\nabla p + \nu \nabla^2 \textbf{v} \Bigr] $$

Now the terms will be rewritten separately, starting with the curl of the time derivative of \\(\textbf{v}\\).

$$ \nabla \times \frac{\partial \textbf{v}}{\partial t} = \frac{\partial}{\partial t} (\nabla \times \textbf{v}) = \frac{\partial \textbf{$\omega$}}{\partial t} $$

Now for the viscous term.

$$ \nabla \times (\nu \nabla^2 \textbf{v}) = \nu \nabla^2 (\nabla \times \textbf{v}) = \nu \nabla^2\textbf{$\omega$} $$

For the pressure term, the curl of a gradient is taking which is always equal to 0. Therefor the pressure drops out of the equation.

$$ \nabla \times (-\frac{1}{\rho}\nabla p ) = 0 $$

The only term that is left to rewrite is the non-linear term.

$$ \nabla \times \Bigl[(\textbf{v} \cdot \nabla)\textbf{v} \Bigr] = \nabla \times \Bigl[\frac{1}{2} \nabla(\textbf{v} \cdot \textbf{v}) + \textbf{$\omega$}\times \textbf{v }\Bigr] = \nabla \times (\textbf{$\omega$}\times \textbf{v}) $$

Putting all these separated terms back together gives the vorticity equation:

$$ \frac{\partial \textbf{$\omega$}}{\partial t} + \nabla \times(\textbf{$\omega$} \times \textbf{v}) = \nu \nabla^2 \textbf{$\omega$} $$

Rewriting the second term of this equation gives another notation of the vorticity equation which is used more often.

$$ \frac{\partial (\textbf{$\omega$})}{\partial t} + (\textbf{v} \cdot \nabla)\textbf{$\omega$} =  (\textbf{$\omega$} \cdot \nabla )\textbf{v} + \nu \nabla^2 \textbf{$\omega$} $$

The above equation states that the vorticity is transported by the fluid velocity (terms on the left), stretched by the fluid velocity gradient (third term), and diffused by viscosity \\(\nu\\) (last term). It can be noticed that this equation is applicable for three-dimensional cases. The difference in 2D and 3D simulations can be seen in the equation below [[5]](http://www.diva-portal.org/smash/get/diva2:205082/fulltext01.pdf). 

$$ \frac{\partial \textbf{$\omega$}}{\partial t} + (\textbf{v} \cdot \nabla)\textbf{$\omega$} =  \nu \nabla^2 \textbf{$\omega$} $$

The term \\((\textbf{$\omega$} \cdot \nabla )\textbf{v}\\) has dropped out. This is called the `vortex stretching` term. The vortex motion in 3-D space differs from 2-D in several ways. The most important result is vortex stretching and the consequent of non-conservation of vorticity. In 2-D flows, the velocity is perpendicular to the vorticity, so \\(\textbf{v}\cdot\textbf{$\omega$}=0\\).

Vortex methods are based on the Lagrangian approach. The Lagrangian time derivative following a fluid particle is defined as follows [[6]](https://web1.eng.famu.fsu.edu/~dommelen/papers/subram/style_a/node15.html):

$$ \frac{D}{Dt} = \frac{\partial}{\partial t} + \textbf{v}\cdot\nabla $$

In terms of this Lagrangian time-derivative, the vorticity equation can be rewritten.

$$ \frac{\partial \textbf{$\omega$}}{\partial t}=\nu \nabla^2 \textbf{$\omega$} $$

According to this equation, the vorticity of a fluid particle changes only due to diffusion. In inviscid flows \\((\nu=0)\\) the vorticity of a fluid particle does not change. This result is very useful in vorticity calculations.

## Simulation
The simulations have been carried out using [Ansys Fluent](https://www.ansys.com/products/fluids/ansys-fluent) CFD software. In this case, a `laminar flow` can be observed. Laminar flow refers to a smooth and orderly flow pattern of a fluid in which the fluid particles move in parallel layers or streamlines without significant mixing or turbulent behavior. Ansys Fluent solves the Navier Stokes equations in every point of the computational domain. The 2D Navier–Stokes equations explain the momentum conservation of incompressible fluid. There are two momentum equations corresponding to the velocity components in the x and y directions of 2D flow. The governing equations corresponding to the conservation of momentum for 2D incompressible flow in differential form can be written as:

$$ u\frac{\partial u}{\partial x} + v\frac{\partial u}{\partial y} = -\frac{1}{\rho}\frac{\partial p}{\partial x}+\mu (\frac{\partial^2 u}{\partial x^2}+\frac{\partial^2 u}{\partial y^2})$$

$$ u\frac{\partial v}{\partial x} + v\frac{\partial v}{\partial y} = -\frac{1}{\rho}\frac{\partial p}{\partial x}+\mu (\frac{\partial^2 v}{\partial x^2}+\frac{\partial^2 v}{\partial y^2})$$

where the first equation describes the momentum in x direction and the second equation describes the momentum in y direction.

#### Computational domain
The computational domain, boundary conditions and mesh were created using the information given in the caption of the picture. Here it is given that a piston drives water with (almost) constant speed normal to the axis of a wedge of 30\degree semi-vertex angle, Reynolds number of order 1000 and the piston stops at 12.5 seconds. One may notice that this is far too little information to create a reliable domain. [Pullin \& Perry 1980](https://authors.library.caltech.edu/91072/1/some_flow_visualization_experiments_on_the_starting_vortex.pdf) have provided a document with more information about the experiments, including some of the dimensions of the experimental setup and of the wedge. The height of the wedge is given to be 12.70 \\(cm\\). The velocity of water had to be obtained from a graph and estimated to be 0.63 \\(cm/s\\). In the computational domain, the walls and the wedge have no-slip boundary conditions applied, while the inlet is specified as a velocity inlet and the outlet as a pressure outlet. In the figures below the computation domain in shown and the experimental setup of [Pullin \& Perry 1980](https://authors.library.caltech.edu/91072/1/some_flow_visualization_experiments_on_the_starting_vortex.pdf) can be seen.

What is interesting about this case, is that after 12.52 seconds the piston stops moving, creating a stopping vortex in front of the wedge tip. In Ansys Fluent this has to be modelled using the 'named expressions' function. In the named expressions tab the change in velocity can be created using an 'IF'-statement as used in coding language `Python`. The expression reads: IF(0 [s] < t < 12.52[s], 0.0063 [m/s], 0 [m/s]). Here it is important to add the units such as \\(s\\) and \\(m/s\\), otherwise `Ansys Fluent` will not recognise the input. It automatically outputs a graph of the created expression.

{{< carousel images="Comp*.jpg" interval="3000">}}



#### Testing hypotheses
From the comparison of the actual pictures and the simulations can be concluded that the they do not match as well as priorly hoped. The edge vortex seems to shed from a greater angle from the wedge than the actual case. Another difference is seen in the distance of the vortex to the wedge. In the simulations the vortex stays relatively in the same place, while in the actual case the vortex seems to travel in positive x-direction and negative y-direction. A number of hypotheses can be tested to see what could possible have been different in the experimental setup that was not stated in the documentation. 

#### Developed Poiseuille flow
The first hypothesis that is tested is: has the computational domain in front of the wedge an impact on the angle or distance of the edge vortex? As can be seen from the figure below a `Poiseuille flow` moves steadily and uniformly along the domains axis, with no variation in the flow direction. The flow is driven by a pressure gradient along the pipe, typically created by a pressure difference between the two ends of the pipe. The fluid velocity follows a parabolic shape. The maximum velocity occurs at the center of the pipe, while the velocity decreases towards the pipe's walls. The velocity profile is symmetric with respect to the pipe's diameter. It could be the case that in the previous computational domain no parabolic velocity profile was developed, causing a difference in vortex angle and distance from the wedge. A computational domain has been chosen so that the distance from inlet to wedge is four times larger than in the initial simulations. 

{{< carousel images="Pois*.jpg" interval="3000">}}

The result can be seen in the figure below. Comparing this picture to the experimental data, it can be concluded that a developed Poisseuille flow does not improve the angle or distance from the wedge. The angle at which the edge vortex sheds has barely changed and the distance of the vortex to the wedge is still the same. The vortex curling also happens later, as this frame is at a time of 7 seconds, while it looks more like the experiment at 1 second.

{{< figure     
    src="PoiseuilleResult.png"
    caption="Result for developed Poiseuille flow at 7seconds."
    alt="Result for developed Poiseuille flow at 7seconds"
    >}}

#### Round tip
The second hypothesis that is tested is: Has the sharpness of the wedge effect on the vortex shedding angle? In the documentation, the sharpness of the wedge is not discussed even though this could majorly impact the simulation. To test this hypothesis, a wedge was made with the same computational domain as the original simulation, but with a round edge instead of a sharp edge. With the `fillet` function in `DesignModeler` a round tip was created with a radius of 0.3 \\(cm\\). The computational domain and the result of the simulation can be seen in the figures below.

{{< figure src="Roundtip1_Domain.PNG" title=">Computational domain for round tip simulation" >}}

Comparing the result to the original simulation it can be seen that there is indeed a change in angle of the edge vortex. The vortex sheds more horizontal, but does not develop further away from the wedge. It can be concluded that a round edge can improve the similarity between experiment and simulation, but since there is no information available about the edge it is difficult to determine the optimal sharpness/roundness. 

{{< figure src="Roundtip2.png" title=">Result for round tip simulation" >}}

#### Higher velocity
Another vital bit of information is the speed with which the piston pushes the water forward. For the experimental setup programs were written to generate piston velocity-time profiles approximated by the following equation:

$$V_p = At^m$$

where \\(V_p\\) is the piston velocity, \\(t\\) is the time and \\(A\\) and \\(m\\) are pre-chosen constants. The nominal values of \\(m\\) chosen were: 0, 0.25, 0.50 and 0.1. The piston distance-time (x,t) profiles were measured for each \\(m\\) using a video system. From this graph the velocity can be obtained for the computational domain that is shown in figure \ref{fig:highvel}. However  'It was found that the actual x, t profiles obtained did not exactly correspond to integration of (1) with the prechosen A, m, presumably owing to accumulation of small errors' [Pullin \& Perry 1980](https://authors.library.caltech.edu/91072/1/some_flow_visualization_experiments_on_the_starting_vortex.pdf). Chances are that the obtained velocity of 0.63 \\(cm/s\\) is not entirely correct. Therefor it is tested if a higher velocity has an impact on the vortex angle and/or distance from the wedge.

{{< carousel images="Vel*.PNG" interval="3000">}}

The results can be seen of a simulation with the same computational domain as in the `Round tip` section, except the velocity is 1 \\(cm/s\\) instead of 0.63 \\(cm/s\\). The choice was made to include the round tip design, as this gave a better approach to the experimental data. Together with the higher velocity is was expected to give a result that would be closer to the experimental pictures. It can be seen that the distance of the vortex to wedge has not changed significantly. Therefor it can be concluded that the difference between experimental and simulation data is not caused by the minor error that comes from obtaining the velocity from the graph.

{{< figure     
    src="HigherVelocityResult.png"
    caption="Paraview pipeline."
    alt="Paraview pipeline"
    >}}

## Visualization
Post-processing of the obtained data from Ansys Fluent is done in [Paraview](https://www.paraview.org/). ParaView is an open source post-processing visualization engine that is widely used in areas from engineering to medical science and many more. The final pictures that were represented at the top do not match as well as was hoped before starting this project. Multiple reasons have already been given in previous sections, however in the post-processing a important dependency is present that can change the visualization tremendously: the location of the paint injections.

#### Location of holes
In the experimental setup the flow details were made visible by the use of a readily available household blue dye injected into the water through small holes at various points on the centre-lines of each wedge model. A small amount of alcohol was used to make a neutrally buoyant readily visible dye mixture. Dye holes were positioned on both the upstream and downstream faces of each model at distances from the wedge apex which varied with the wedge geometry [Pullin \& Perry 1980](https://authors.library.caltech.edu/91072/1/some_flow_visualization_experiments_on_the_starting_vortex.pdf). In the citation, no information is given on where the holes are located exactly. Therefor, trial and error is used to find the optimal position of two `line sources` that best represent the experimental case. Deviations from the experimental data is inevitable.

Two line sources are created, resembling the injection holes of the paint. These line sources can be placed anywhere on the computational domain. The coordinates of the two line sources can be seen in the figures below. The property `resolution` sets the number of sampling points uniformly. Setting this property to 100 means generating 100 sampling points uniformly distributed over the line source.

{{< carousel images="Vis*.jpg" interval="3000">}}

<div class="row">
  <div class="column">
    <figure src="Vis1_Line1.jpg" alt="line1" style="width:100%">
  </div>
  <div class="column">
    <figure src="Vis1_Line1.jpg" alt="line2" style="width:100%">
  </div>
</div>

It may be noticed that in the 13second comparison pictures the stopping vortex does not look similar to the experimental data. It could, however, be improved by changing the location of the line sources. The choice was made to not do this, as in the experimental setup the injection holes do not change either for different timesteps. An improved visualization can be seen, where the line sources have been altered to resemble the experimental picture as well as possible. 

{{< figure     
    src="13secS.jpg"
    caption="Result for improved line source placement at 13s"
    >}}

#### ParticleTracer
The particle tracer filter in ParaView creates massless particle tracking with pathlines that animate over time. To use the particle tracer for visualizing the vortex, apply the particle tracer filter taking the Ansys Fluent data as `input` and your source object (the two line sources) as `source`. The option `Force Reinjection Every NSteps` gives the injection rate per time step (0: injects only at the beginning; 1: injects every time step).

{{< figure     
    src="Paraview.jpg"
    caption="Paraview pipeline."
    alt="Paraview pipeline"
    width="50%"
    >}}

#### No-slip condition
Several hypotheses have been tested to find the reason behind why the pictures do not match completely in the previous sections. These reasons all had to do with the given information not being extensive enough to create the identical conditions. There is another reason that the pictures do not match completely due to a reason that has nothing to do with given information. This is the `no-slip` wall condition used in Ansys Fluent. It assumes that the speed of the fluid layer in direct contact with the boundary is identical to the velocity of this boundary. There is no relative movement between the boundary and this fluid layer, therefore there is no slip. A suggestion to decreasing the gap between the line source and wedge would be to increase the size of the `ParticleTracer`. However, since the paint layer is so thin, it is important to keep the particles small. It is also not possible to change the no-slip condition to slip condition, as this would not allow for boundary layer development around the wedge.
