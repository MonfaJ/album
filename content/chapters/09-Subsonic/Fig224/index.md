---
title: "Fig 224. Projectile at near-sonic speed "
date: 2024-05-01
weight: 253
featured: true
tags: ["Supersonic", "Ansys Fluent", "Transonic"]
authors:
  - "wouterlitjens"
---


{{< katex >}}


{{< slider "original0978.jpg" "M0978.png" "Experiment" "Simulation">}}
$$
\vcenter{M = 0.978}
$$
{{< slider "Featured.jpg" "M0990.png" "Experiment" "Simulation">}}
$$
\vcenter{M = 0.990}
$$
*"Still closer to the speed of sound, the shock-wave pattern of the preceding pages had spread laterally to great distances. These two photographs are from the same firing, so that the second one was actually taken earlier in  the trajectory"* Photographs by A. C. Charters, in von Kármán 1947


## Theory
These simulations are part of a series together with [figure 223]({{< ref "/chapters/09-Subsonic/Fig223" >}}) and [figure 253]({{< ref "/chapters/11-Supersonic/Fig253" >}}) all done in transonic flow. The shockwaves and convex corner flow around the bullet make for switches between subsonic and supersonic flow. Here the main challenges of simulating transonic flow are discussed.




#### The Transonic flow problem
For transonic flow the change in velocity around Mach 1 complicated early CFD computations. This can be seen in the non-linear perturbation velocity potential equation for transonic flow, which was used back in the day and follows from the definition of the velocity potential \\(\phi\\) for 2D:
$$
\mathbf{V} = \nabla \phi \\\\
u = V_{\infty} + \hat{u} \\\\
v = \hat{v}
$$
With u and v being cartesian velocity components in x and y direction respectively, V being the velocity magnitude, \\(V_{\infty}\\) flow field velocity and \\(\hat{u}\\) and \\(\hat{v}\\) being velocity perturbations (increments).
{{< figure    
    src="perturbations.png"
    caption="Uniform to perturbed flow [1]"
    alt="perturbed."
    >}}


Using \\(\phi\\) to obtain one equation that represents a combination of the continuity, momentum and energy equations is useful to use one governing equation with one unknown. This creates the final equation:


$$
(1-M_{\infty}^2) \frac{\partial^2 \hat{\phi}}{\partial x^2} + \frac{ \partial^2 \hat{\phi}}{\partial y^2} = M_{\infty}^2 [(\gamma + 1)\frac{\partial \hat{\phi}}{\partial x}\frac{1}{V_{\infty}}] \frac{\partial^2 \hat{\phi}}{\partial x^2}
$$
It is important to note here that this equation approximates the physics of a steady, compressible, inviscid 2D flow. The important point here is the factor (\\(1-M_{\infty}^2\\)), which will be higher than 0 in subsonic flow and lower than 0 for supersonic flow and result in a different form of partial differential equation. In supersonic flow, it is a hyperbolic partial differential equation while in subsonic it is an elliptic partial differential equation, which is a big difference in math type.  This shows a big difference in physics between subsonic and supersonic flow.


This equation was used in the earliest transonic CFD simulations. Later Euler equations were used, which predict shockwaves pretty well, but do not fare well in transonic flow due to shock wave interaction with the boundary layer, which needs viscosity. For a comparison a look can be taken at [Figure 223]({{< ref "/chapters/09-Subsonic/Fig223" >}}). The current state of the art for transonic flow is solving the Navier-Stokes equations with the viscous term. Here the turbulence model frequently being the Achilles heel of these complicated calculations [1].


#### Transonic buffet
In transonic flow, the interactions between shock waves and separated shear layers result in self-sustained low-frequency oscillations of the shock wave also called transonic buffet. Some aspects of these oscillations are still incomprehensible [2]. A transient computational setup with LES is needed to show these oscillations and comes at a high cost. The steady-state simulations done estimate the average position.






#### Numerical dissipation
CFD simulations currently in transonic flow are very sensitive to artificial dissipation characteristics of the computational fluid algorithm. With a bad algorithm problems such as difficult convergence, poor reliability and difficulty to accurately simulate shock position will occur [3].


Numerical dissipation is an artificial effect that is introduced when solving the governing equations, which acts as an unnatural damping of the variables at play. This with as main goal to damp oscillations and smooth out high-frequency noise and thus stabilizing the solution ensuring convergence [4]. Turbulence models and mesh refinement around areas with steep gradients lower the amount of numerical dissipation needed.


<!-- #### Numerical dissipation can lead to the following effects:
- Smeared Shocks: The sharp dicontinuities get smeared over the shockwaves, as a result inacurracies in the location and strength of the shocks occur.
- Boundary layer: Artificial thickening of the boundary layer and dempening of turbulance.
- Shock-Boundary Layer Interaction: The change in shockwaves, but also change in boundary layer will weaken this interaction, with inaccurate modeling as result [(Figure 253)]({{< ref "/chapters/11-Supersonic/Fig253" >}}).
- Stabilization of flow instabilities: In transonic flow some instabilities can develop between subsonic and supersonic regions. Numerical dissipation can stabilize this flow that does not capture its unsteady nature.


To optimise for the effects of numerical dissipation different things can be done. First, higher-order numerical schemes can be used. Further, adaptive mesh refinement can be used at places with steep gradients. Finally, as previously mentioned: A good turbulence model that can account for shock-boundary layer interaction, which reduces artificial dampening.


Supersonic compared to subsonic flow
As shown by the velocity potential equation above there are big differences in the physics between supersonic and subsonic flow. The most noticable is shockwaves which can be read about in [Figure 253]({{< ref "/chapters/11-Supersonic/Fig253" >}}), but there are also other factor as play.


One of these changes is that in a diverging tube supersonic flow accelerates, in contrast to subsonic which slows down. This can be attributed to the conservation of mass equation \\(\\rho v dA = constant \\), conservation of momentum \\(\rho v dv = -p\\), isentropic flow relation \\(\frac{dp}{p}=\gamma \frac{d\rho}{\rho} \\) and the equation of state \\(\frac{p}{\rho} = RT\\)can be written as:
$$
(1-M^2)\frac{dv}{v}=-\frac{dA}{A}
$$
Using \\(M = \frac{v}{a} \\) and \\(a = \gamma RT\\) where a is the velocity of sound. This equation shows how the velocity changes with an area change depending on the mach number. If subsonic a flow increase lead to negative dv and if supersonic to a positive dv.
(https://www1.grc.nasa.gov/beginners-guide-to-aeronautics/nozzle-design/)


Another significant difference is in adiabatic flow where friction in a duct is different in both situations, which can be explained with Fanno flow. The T-s diagram of the flow, with a line from low to high velocity flow, looks like the following:
{{< figure    
    src="Fanno flow.png"
    caption="Fanno curve on a h-s diagram [2]"
    alt="Rayleigh curve."
    >}}
At point S with the maximum entropy of the flow the Mach number equals 1. Above point S the flow is subsonic and down it is supersonic. Friction causes the downstream of the flow to move closer to point S, which means that for subsonic flow the velocity increases, while decreasing for supersonic flow.


Finally there is also Reyleigh flow, which talks about heat addition with a similar curve as the Fanno curve:
{{< figure    
    src="Rayleigh curve.png"
    caption="Rayleigh curve on a h-s diagram [2]"
    alt="Rayleigh curve."
    >}}
This curve says something about the flow reacting to heat being added. A subsonic flow speeds up when it is heated, while as supersonic flow slows down. -->


## Simulation & Visualization
About the simulation and visualization can be read in [figure 253]({{< ref "/chapters/11-Supersonic/Fig253" >}}).


<!-- #### Domain
In the original picture a 155mm artillery shell is used. While making the domain the shape was traced from the original image, and small details on the body were added based on the shock- and expansion waves. For example, a small increase in diameter at the middle of the shell is found based on expansion waves, and at the back a small dimple is found, based on double expansion waves


{{< carousel images="shape*.png" interval="3000">}}


At the back body there is suspected to be a hole. This is based on 155mm artillery and iterating over CFD simulations. The cutout significantly lowers the collision point of the back expansion- and shockwave in transonic flow, in comparison to no hole.


Then an axisymmetric simulation is formed by using an axis boundary at the x-axis, a no slip wall boundary condition for the object, and a farfield boundary for the other three bounderies. Gauge pressure and temperature have been found irrelevant to the shape of the waves, as the value ratios stay about the same.




#### Meshing
The start mesh size is 0.0055 m and mainly quadrileteral elements. After running the simulation, the mesh is refined by using ansys refinement tools around density changes and refined again around the shell body.
- standard quad mesh
- refined


#### Fluid model
- density based compressible
- air is used ideal gas law
- Sutherland viscosity
- k-epsilon viscosity model reference Fig-47


The computational models used are density based, as the shockwaves are compressible. Because of compressibility the ideal gas law is used for air, as an extra equation on top of the compressible Navier-Stokes equations to solve for four unknowns.


For viscosity the realizable k-epsilon model is used, as viscous forces are rarely dominant in the Navier-Stokes equations due to high velocity. More on viscosity models can be found on Fig-47. Sutherland's Law is used for the viscosity of the air.


## Visualization




[Paraview](https://www.paraview.org/) is used for visualization, using two `gradient` filters on density to create a second order gradient in the horizontal direction. This to create the dark to white shockwaves in the original image. The gradient filter has some opacity, and the colour scale values have been altered to fit the replicated image. The `Transform` filter is used to rotate the object around the z-axis to fit the original image.


{{< figure    
    src="screen1_filters.png"
    caption="Paraview filters"
    alt="Paraview filters"
    >}}
    [2] https://courses.ansys.com/wp-content/uploads/2020/12/Fluids_S1LT3C4L2-HandoutSlides.pdf
    -->


## References
- [1] Fundamentals-of-aerodynamics-6-Edition.pdf
- [2] https://www.sciencedirect.com/science/article/pii/S0376042117300271
- [3] https://ieeexplore.ieee.org/document/10082199
- [4] https://www.nas.nasa.gov/assets/nas/pdf/ams/2018/introtocfd/Intro2CFD_Lecture7_Zingg.pdf)
 

