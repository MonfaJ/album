---
title: "Fig 223. Projectile at high subsonic speeds"
date: 2024-05-01
weight: 253
featured: true
tags: ["Subsonic", "Ansys Fluent", "Euler equations"]
authors:
  - "wouterlitjens"
---


{{< katex >}}


{{< slider "original0840.jpg" "M0840.png" "Experiment" "Simulation">}}
$$
\vcenter{M = 0.840}
$$
{{< slider "original0885.jpg" "M0885.png" "Experiment" "Simulation">}}
$$
\vcenter{M = 0.885}
$$
{{< slider "Featured.jpg" "M0900.png" "Experiment" "Simulation">}}
$$
\vcenter{M = 0.900}
$$
{{< slider "original0946.jpg" "M0946.png" "Experiment" "Simulation">}}
$$
\vcenter{M = 0.946}
$$
{{< slider "original0971.jpg" "M0971.png" "Experiment" "Simulation">}}
$$
\vcenter{M = 0.971}
$$
*"The spark shadowgraphs on these two pages have been arranged to show the shock-wave pattern growing into the subsonic field around a model of an artillery shell as its Mach number is increased. The shell is in free flight through the atmosphere at less than \\(1.5^{\circ}\\) incidence. These five photographs are from four different firings, in each of which the Mach number is gradually decreasing as the shell decelerates."* Photographs by A. C. Charters, in von Kármán 1947


## Theory


#### The Euler equations
Flow is defined with the Navier-Stokes equations, which is a set of functions describing the conservation of mass, momentum and energy:
$$
\begin{cases}
\frac{\partial \rho}{\partial t} + \nabla \cdot (\rho \mathbf{u}) = 0 \\\\
\rho(\frac{\partial \mathbf{u}}{\partial t} + \mathbf{u}
\cdot \nabla \mathbf{u})= - \nabla p + \nabla \cdot \{ \mu[\nabla \mathbf{u} + (\nabla \mathbf{u})^T - \frac{2}{3}(\nabla \cdot u) I]+\zeta(\nabla \cdot \mathbf{u})I\} + \rho g  \\\\
\frac{\partial e}{\partial t} + \mathbf{u} \cdot \nabla e = - \frac{p}{\rho} \nabla \cdot \mathbf{u}
\end{cases}
$$


To solve this set of equations is computationally heavy which is why simplifications are used, in some cases with high velocities it could be argued that the viscosity \\(\mu\\) and bulk viscosity \\(\zeta\\) are negligible and thus left out of the equation leaving the Euler equations:
$$
\begin{cases}
\frac{\partial \rho}{\partial t} + \nabla \cdot (\rho \mathbf{u}) = 0 \\\\
\rho(\frac{\partial \mathbf{u}}{\partial t} + \mathbf{u}
\cdot \nabla \mathbf{u})= - \nabla p + \rho g  \\\\
\frac{\partial e}{\partial t} + \mathbf{u} \cdot \nabla e = - \frac{p}{\rho} \nabla \cdot \mathbf{u}
\end{cases}
$$
These equations have four unknowns, thus to solve for a compressible gas the ideal gas law is used as an extra equation.

## Simulation
#### Euler equations compared to k-epsilon model
The simulations above used the k-epsilon model but were also run without a viscosity model. The following results were obtained:
{{< slider "M0840.png" "M0840Euler.png" "k-epsilon" "Euler">}}
$$
\vcenter{M = 0.840}
$$
{{< slider "M0885.png" "M0885Euler.png" "k-epsilon" "Euler">}}
$$
\vcenter{M = 0.885}
$$
{{< slider "M0900.png" "M0900Euler.png" "k-epsilon" "Euler">}}
$$
\vcenter{M = 0.900}
$$
{{< slider "M0946.png" "M0946Euler.png" "k-epsilon" "Euler">}}
$$
\vcenter{M = 0.946}
$$
{{< slider "M0971.png" "M0971Euler.png" "k-epsilon" "Euler">}}
$$
\vcenter{M = 0.971}
$$
The first thing that can be seen is that with the Euler equation, the shockwaves are bigger at lower Mach. This is probably due to the no-slip wall and therefore higher flow velocity at the wall. Further, the wake downstream of the artillery shell is way smaller for the Euler simulations. This is due to the wake being a form of flow separation and turbulence, the last of which is not modelled in the Euler equations.


## Simulation set-up & Visualization
The simulation set-up and visualization can be read in [figure 253]({{< ref "/chapters/11-Supersonic/Fig253" >}}).
